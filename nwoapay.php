<?php
/**
 * @package nwoapay
 * @version 0.3.0
 */

/*
Plugin Name: NWOA Payment Module
Plugin URI: https://gitlab.com/mwhipple/nwoa-pay
Description: Integration for nwoa.com WordPress site with CyberSource hosted payment processing
Author: Matt Whipple
Version: 0.3.0
Author URI: http://www.mattwhipple.com
License: Apache-2.0
License URI: http://www.apache.org/licenses/LICENSE-2.0
*/

/**
 * Copyright 2015 Matt Whipple (email: matt at mattwhipple.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Some constants are defined here to be used throughout
 */
define( 'NWOAPAY_VERSION'             , '0.1.0'              );
define( 'NWOAPAY_REQUIRED_WP_VERSION' , '3.2'                );
define( 'NWOAPAY_NAME'                ,	'nwoapay'            );
define( 'NWOAPAY_TITLE'               ,	'NWOA Payment Module');
define( 'NWOAPAY_PATH'                , NWOAPAY_NAME         );

/**
 * Provider for key information used to authenticate CyberSource requests
 */
interface NwoaKeyProvider {
    public function getProfileName();
    public function getAccessKey();
    public function getSecretKey();
}

/**
 * Holder for singleton NwoaKeyProvider used as a global container for
 * an overridable implementation
 */
class NwoaKeyProviderHolder {
    static $instance;
}

/**
 * NwoaKeyProvider implementation which uses WordPress Options API and DB
 */
class OptionsNwoaKeyProvider implements NwoaKeyProvider {
    public function getProfileName() {
        $options = get_option('nwoapay-settings');
        return $options['profile-name'];
    }

    public function getAccessKey() {
        $options = get_option('nwoapay-settings');
        return $options['access-key'];
    }

    public function getSecretKey() {
        $options = get_options('nwoapay-settings');
        return $options['secret-key'];
    }
}

NwoaKeyProviderHolder::$instance = new OptionsNwoaKeyProvider();
/**
 * Activation/Deactivation
 */
register_activation_hook( __FILE__, 'nwoapay_activate' );
function nwoapay_activate() {
    add_option('nwoapay-settings',
               array('access-key'    => 'Get from CyberSource SA Profile!',
                     'secret-key'    => 'Get from CyberSource SA Profile!',
                     'keep-settings' => false) );
}

register_uninstall_hook( __FILE__, 'nwoapay_uninstall' );
function nwoapay_uninstall() {
    $options = get_options('nwoapay-settings');
    if (!$options['keep-settings'])
        delete_option('nwoapay-settings');
}

/**
 * Ajax Form signing, expecting a JSON payload of the form that needs a
 * signature, will return signature text to be handled by FE
 */
add_action('wp_ajax_nopriv_nwoapay_get_signature', 'nwoapay_get_signature');
add_action('wp_ajax_nwoapay_get_signature', 'nwoapay_get_signature');

function nwoapay_get_signature( ) {
    $response = new WP_Ajax_Response;

    //php://input is a type of STDIN used for HTTP payloads
    $payload = json_decode(file_get_contents('php://input'), true);

    $options = get_option('nwoapay-settings');
    $response->add( array('data' =>
                          sign($payload, NwoaKeyProviderHolder::$instance->getSecretKey()) ));
    $response->send();

    exit();
}

/**
 * Load scipt dependencies
 */
add_action('wp_enqueue_scripts', 'nwoapay_enqueue_scripts');
function nwoapay_enqueue_scripts() {
	wp_enqueue_script('json-polyfill', 
                      plugins_url(NWOAPAY_PATH.'/js/json3.min.js') );
    wp_enqueue_script('jquery-validate',
                      plugins_url(NWOAPAY_PATH.'/js/jquery.validate.min.js'),
                      array('jquery') );
    wp_enqueue_script('validate-methods',
                      plugins_url(NWOAPAY_PATH.'/js/additional-methods.min.js'),
                      array('jquery', 'jquery-validate') );
    wp_enqueue_script('nwoapay',
                      plugins_url(NWOAPAY_PATH.'/js/nwoapay.js'),
                      array('validate-methods') );

    // Get current page protocol
    // TODO: presumably protocol-relative doesn't work?
    $protocol = isset( $_SERVER["HTTPS"] ) ? 'https://' : 'http://';

    // Output admin-ajax.php URL with same protocol as current page
    $params = array(
        'ajaxurl' => admin_url( 'admin-ajax.php', $protocol ) );
    wp_localize_script( 'nwoapay', 'nwoapay', $params );
}

add_shortcode('nwoapay_link', 'nwoapay_link_expand');
function nwoapay_link_expand($atts, $content = null, $tag = '') {
    return "<a href='".admin_url('admin-ajax.php').
                      "?action=".NWOAPAY_ORDER_FORM_ACTION.
                      "' class='nwoaPayPop'".
                      " data-width='550' data-height='700'>".$content."</a>";
}

$orderForm = new NwoaPayOrderForm();
add_shortcode('nwoapay_form' , array($orderForm, 'expand_form' ));
add_shortcode('nwoapay_field', array($orderForm, 'expand_field'));
class NwoaPayOrderForm {
    public function expand_field($atts, $content = null, $tag = '') {
        $a = array_merge( array(
            'type' => 'text'
        ), $atts);
        $this->addSignedField($a['name']);
        $control = '<input ';
        foreach ($a as $key => $value) {
            $control .= " $key=\"$value\"";
        }
        $control .= '/>';
        return $control;
    }

    public function expand_form($attrs, $content, $tag = '') {
        //evaluate inner content first to collect additional signed fields
        $content = do_shortcode($content);

        $options = get_option('nwoapay-settings');
        $form  = '<form method="post" id="payForm"'.
               'action="https://secureacceptance.cybersource.com/pay">';
        $form .= '<input type="hidden" name="signature"/>';
        $form .= sprintf('<input type="hidden" name="%s" value="en"/>',
                         $this->addSignedField('locale'));
        $form .= sprintf('<input type="hidden" name="%s" value="%s"/>',
                         $this->addSignedField('access_key'),
                         NwoaKeyProviderHolder::$instance->getAccessKey() );
        $form .= sprintf('<input type="hidden" name="%s" value="%s"/>',
                         $this->addSignedField('profile_id'),
                         NwoaKeyProviderHolder::$instance->getProfileName() );
        $form .= sprintf('<input type="hidden" name="%s" value="%s"/>',
                         $this->addSignedField('signed_date_time'),
                         gmdate('Y-m-d\TH:i:s\z'));
        $form .= sprintf('<input type="hidden" name="%s" value="%s"/>',
                         $this->addSignedField('transaction_uuid'),
                         uniqid());
        $form .= sprintf('<input type="hidden" name="%s" value="sale"/>',
                         $this->addSignedField('transaction_type'));
        $form .= sprintf('<input type="hidden" name="%s" value="USD"/>',
                         $this->addSignedField('currency'));
        $form .= sprintf('<input type="hidden" name="%s" value="%s"/>',
                         'signed_field_names',
                         implode($this->signedFields, ','));
        $form .= '<input type="hidden" name="unsigned_field_names" value=""/>';
        $form .= $content;
        $form .= '</form>';
        return $form;
    }

    private $signedFields = [];
    private function addSignedField($name) {
        array_push($this->signedFields, $name);
        return $name;
    }

}

/**
 * An Ajax function that serves the initial order form.
 * This should optimally be moved into a new theme page,
 * but this is the fastest initial approach
 */
define('NWOAPAY_ORDER_FORM_ACTION', 'nwoapay_order_form');
add_action('wp_ajax_nopriv_'.NWOAPAY_ORDER_FORM_ACTION, 'nwoapay_order_form');
add_action('wp_ajax_'.NWOAPAY_ORDER_FORM_ACTION, 'nwoapay_order_form');

function nwoapay_order_form() {
    $context = array(
        "resourcePath" => plugins_url(NWOAPAY_PATH));
    ob_start();
    require plugin_dir_path(__FILE__).'/templates/order_form.php';
    //The clean and echo effectively replaces the buffer contents
    echo do_shortcode(ob_get_clean());
    ob_flush();
    die();
}

/**
 * Admin settings page
 */
class NwoaPaySettingsPage {
    private $options;

    // this seems to be a gross violation of good OOP principles, but the
    // class is more of bundled functionality than anything SOLID
    public function __construct() {
        add_action( 'admin_menu', array($this, 'add_to_menu') );
        add_action( 'admin_init', array($this, 'init') );
    }

    public function init() {
        register_setting('nwoapay-group',
                         'nwoapay-settings',
                         array($this, 'sanitize'));

        add_settings_section('nwoapay-section',
                             'General',
                             '',
                             'nwoapay-settings-page');

        add_settings_field('profile-name',
                           'Secure Acceptance Profile',
                           array($this, 'render_profile_name'),
                           'nwoapay-settings-page',
                           'nwoapay-section');

        add_settings_field('access-key',
                           'Access Key',
                           array($this, 'render_access_key'),
                           'nwoapay-settings-page',
                           'nwoapay-section');

        add_settings_field('secret-key',
                           'Secret Key',
                           array($this, 'render_secret_key'),
                           'nwoapay-settings-page',
                           'nwoapay-section');

        add_settings_field('keep-settings',
                           'Keep Settings on Plugin Removal',
                           array($this, 'render_keep_settings'),
                           'nwoapay-settings-page',
                           'nwoapay-section');
    }

    public function sanitize($input) {
        error_log(print_r($input, true));
        return array(
            'profile-name' => $input['profile-name'],
            'access-key'   => $input['access-key'],
            'secret-key'   => $input['secret-key'],
            'keep-settings'=> $input['keep-settings'] );
    }

    public function add_to_menu() {
        add_options_page( 'Payment Module Settings',
                          'Payment Module',
                          'manage_options',
                          'nwoapay-admin',
                          array($this, 'render_page') );
    }

    public function render_page() {
        $this->options = get_option('nwoapay-settings');
        ?>
        <div class="wrap">
          <h2>Payment Module Settings</h2>
          <form method="post" action="options.php">
          <?php
        settings_fields('nwoapay-group');
        do_settings_sections('nwoapay-settings-page');
        submit_button();
          ?>
          </form>
        </div>
        <?php
    }

    private function text_input_for_setting($setting) {
        printf('<input type="text" id="%s" name="%s" value="%s"/>',
               $setting,
               "nwoapay-settings[$setting]",
               isset($this->options[$setting]) ? esc_attr($this->options[$setting])
                                               : '');
    }

    public function render_profile_name() {
        $this->text_input_for_setting('profile-name');
    }

    public function render_access_key() {
        $this->text_input_for_setting('access-key');
    }

    public function render_secret_key() {
        $this->text_input_for_setting('secret-key');
    }

    public function render_keep_settings() {
        printf('<input type="checkbox" id="%s" name="%s" value="1"%s/>',
               'keep-settings',
               'nwoapay-settings[keep-settings]',
               $this->options['keep-settings'] ? ' checked' : '');
    }

}
// instatiate if potentially needed, triggering registration in constructor
// the design around this seems rough but this also seems like a questionable best practice
// why bother with the check to avoid attaching to hooks that presumably wouldn't get called?
// if that does somehow make sense an encapsulated factory approach would likely be preferable
if (is_admin()) new NwoaPaySettingsPage();

/*
 * Standard Cybersource functions inlined as they don't contain any sensitive info
 */
define ('HMAC_SHA256', 'sha256');

function sign($params, $key) {
    return signData(buildDataToSign($params), $key);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
    $signedFieldNames = explode(",",$params["signed_field_names"]);
    foreach ($signedFieldNames as &$field) {
        $dataToSign[] = $field . "=" . $params[$field];
    }
    return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}


?>