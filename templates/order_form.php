<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta http-euiv="X-UA-Compatible" content="ID=edge,chrome=1">
    <?php
      nwoapay_enqueue_scripts();
      wp_head();
      ?>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maxmum-scale=1"/>

    <title>NEWTON WELLESLEY ORTHOPEDIC Online Payment</title>
    <link rel="stylesheet" href="<?php echo $context['resourcePath']?>/css/payment.css"/>

    <style>
      .error { color: red; font-style: italic; }
    </style>
  </head>

  <body id="notinpaymentflow">
    <div id="header">
      <div class="container group">
        <img alt="NWOA" class="center"
             src="<?php echo $context['resourcePath']?>/images/nwoa_logo.png" />
      </div>
    </div>
    <div id="notification_logo">
      <div class="container group">
        <img alt="NWOA" class="left"
             src="<?php echo $context['resourcePath']?>/images/nwoa_logo.png" />
      </div>
    </div>

    <div class="container group">
          [nwoapay_form]
        <div class="left_column updateCheckBox" id="account_details">
          <h1>Invoice Information</h1>
          <div id="required_field_explanation">
            <span id="required_field_char"> * </span> Requred field
          </div>
          <p>Please provide the information from your bill below
            and you can proceed to our secure online payment process.</p>

          <fieldset>
            <legend><span>Account and Amount</span></legend>
            <ol>
              <li>
                <label for="reference_number">Account Number <em>(11 digits including an 'A')</em> * </label>
          [nwoapay_field name="reference_number"
           aria-required="true" class="required" type="text"
           id="refrence_number"/]
              </li>
              <li>
                <label for="amount">Amouny To Pay *</label>
          [nwoapay_field name="amount"
           aria-required="true" class="required"
           id="amount"/]
              </li>
            </ol>
          </fieldset>
          <div class="submit-container">
            <input type="submit" id="paySubmit"
                   value="Proceed to Payment >>"
                   class="button-primary submitButtonText right pay_button"/>
          </div>
        </div>
          [/nwoapay_form]
    </div>
  </body>
</html>


