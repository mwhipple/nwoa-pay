/**
 * The JavaScript used for the payment integration. Provides some basic
 * client-side validation and also intercepts the form submission so that
 * the signature can be retrieved through an AJAX request and added
 * to the data that is sent to the hosted page.
 */
(function($) {
    /**
     * This is a standard borrowed jQuery function which serializes a form into
     * a JS object rather than an array. It is used in the AJAX POST below to
     * transfer the form data as the payload so that the backend can generate the
     * signature
     */
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            }
            else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    /**
     * Add the payment form behavior (validation and signing) to payForm$ 
     * The form is expected to have a name=amount, #paySubmit, and name=signature
     */
    var wirePaymentForm = function(payForm$) {
        /*
         * Validation: Amount is required and must be in format below
         */
        $.validator.addMethod("validAmount", function(value, element, params) {
            return value.match(/\d+\.\d{2}/);
        }, "Please enter the amount in ###.## format");
        $.validator.addMethod('validAccount', function(value, element, params) {
            return value.match(/\d{6}A\d{4}/);
        }, "Please enter a valid 11 character account number ending in A####");

        payForm$.validate({
	    rules: {
	        amount:           { required: true, validAmount: true },
                reference_number: { required: true, validAccount: true},
            }
        });

        /**
         * Retrieve signature through AJAX call and add to form before submit
         */
        var signAndSubmit = function() {
            $.ajax({
                type: "POST",
                contentType: "application/json",
                  //WP ajax endpoint corresponding to PHP function
                url: nwoapay.ajaxurl + "?action=nwoapay_get_signature",
                //jQuery foolishness to use JSON
                data: JSON.stringify(payForm$.serializeObject()),
                //Type of return, matches WP convention
                dataType: "xml"
            }).done(function(data) {
                payForm$.find("[name='signature']")
                //Extract data child from XML
                    .val($("response_data", data).text());
                //Get DOM Form from wrapper and call submit
                payForm$.get(0).submit();
            });
        };

        /**
         * Override default submit to validate and populate signature
         */
        payForm$.on("click", "#paySubmit", function(event) {
            event.preventDefault();
            payForm$.validate();
            if (payForm$.valid()) {
                signAndSubmit();
            }
        });
    };

    //Wire payment form and add behavior to initial button
    $(function() {
        //Wire payment form if available
        wirePaymentForm($("#payForm"));

        //Modify payment link to trigger popup rather than having
        // standard behavior, relies on the href attribute which should be set to:
        /// ./wordpress/wp-admin/admin-ajax.php?action=nwoasa_order_form
        var paymentLinks$ = $(".menu-item-1579").add(".nwoaPayPop");
        paymentLinks$.click(function(event) {
            event.preventDefault();
            window.open(event.currentTarget.href, 'paypop');//, 'width=700,height=700');
        });
    });

})(jQuery);
